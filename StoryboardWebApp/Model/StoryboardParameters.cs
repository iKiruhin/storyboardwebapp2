﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoryboardWebApp.Model
{
	public class StoryboardParameters
	{
		public float Size { get; set; }

		/// <summary>
		/// Отступ сверху.
		/// </summary>
		public int PaddingTop { get; set; }

		/// <summary>
		/// Отступ справа (в пикселях).
		/// </summary>
		public int PaddingRight { get; set; }

		/// <summary>
		/// Отступ снизу.
		/// </summary>
		public int PaddingBottom { get; set; }

		/// <summary>
		/// Отступ слева.
		/// </summary>
		public int PaddingLeft { get; set; }



		public static StoryboardParameters PaddingsComplex
		{
			get
			{
				return new StoryboardParameters()
				{
					Size = 400,
					PaddingTop = 50,
					PaddingRight = 100,
					PaddingBottom = 150,
					PaddingLeft = 200
				};
			}
		}
		public static StoryboardParameters Paddings10
		{
			get
			{
				return new StoryboardParameters()
				{
					Size = 600,
					PaddingTop = 10,
					PaddingRight = 10,
					PaddingBottom = 10,
					PaddingLeft = 10
				};
			}
		}
		public static StoryboardParameters Paddings_0x10
		{
			get
			{
				return new StoryboardParameters()
				{
					Size = 500,
					PaddingTop = 0,
					PaddingRight = 10,
					PaddingBottom = 0,
					PaddingLeft = 10
				};
			}
		}
		public static StoryboardParameters Paddings_10x0
		{
			get
			{
				return new StoryboardParameters()
				{
					Size = 500,
					PaddingTop = 10,
					PaddingRight = 0,
					PaddingBottom = 10,
					PaddingLeft = 0
				};
			}
		}
		public static StoryboardParameters Paddings100
		{
			get
			{
				return new StoryboardParameters()
				{
					Size = 600,
					PaddingTop = 100,
					PaddingRight = 100,
					PaddingBottom = 100,
					PaddingLeft = 100
				};
			}
		}
		public static StoryboardParameters PaddingsCustom
		{
			get
			{
				return new StoryboardParameters()
				{
					Size = 400,
					PaddingTop = 10,
					PaddingRight = 20,
					PaddingBottom = 30,
					PaddingLeft = 40
				};
			}
		}

		public static StoryboardParameters Paddings0
		{
			get
			{
				return new StoryboardParameters()
				{
					Size = 500,
					PaddingTop = 0,
					PaddingRight = 0,
					PaddingBottom = 0,
					PaddingLeft = 0
				};
			}
		}



		internal object ToString(string objectMainParamName)
		{
			return string.Format(@"<strong>{0}</strong><br />
Size ({6}): {1}<br />
PaddingTop: {2}<br />
PaddingRight: {3}<br />
PaddingBottom: {4}<br />
PaddingLeft: {5}<br />"
				, this.GetType().Name, Size, PaddingTop, PaddingRight, PaddingBottom, PaddingLeft, objectMainParamName);
		}

	}
}