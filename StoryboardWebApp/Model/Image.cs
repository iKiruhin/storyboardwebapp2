﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace StoryboardWebApp.Model
{
	public class Image : BaseStoryboardObject
	{
		System.Drawing.Image imageFile;
		string argumentExceptionMessage = "недопустимое свойство у экземпляра Image";



		public string Url { get; set; }



		protected override float calculatedSize
		{
			get { throw new ArgumentException(argumentExceptionMessage); }
		}
		
		
		
		float _outerWidth = 0;
		public override float Width
		{
			get
			{
				if (_outerWidth == 0)
				{
					parent.SetWidthHeightForChild(this, out _outerWidth, out _outerHeight);
				}
				return _outerWidth;
			}
		}

		float _outerHeight = 0;
		public override float Height
		{
			get
			{
				if (_outerHeight == 0)
				{
					parent.SetWidthHeightForChild(this, out _outerWidth, out _outerHeight);
				}
				return _outerHeight;
			}
		}

		public override string GivenSizeName
		{
			get { throw new ArgumentException(argumentExceptionMessage); }
		}
		public override string CalculatedSizeName
		{
			get { throw new ArgumentException(argumentExceptionMessage); }
		}

		public override float InnerWhRelation
		{
			get
			{
				return (float)imageFile.Width / imageFile.Height;
			}
		}
		public override float InnerHwRelation
		{
			get
			{
				return (float)imageFile.Height / imageFile.Width;
			}
		}

		public override float OuterWhRelation
		{
			get
			{
				return Width / Height;
			}
		}
		public override float OuterHwRelation
		{
			get
			{
				return Height / Width;
			}
		}



		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="title"></param>
		public Image(string url)
		{
			this.Url = url;
			string imagePath = string.Format(@"{0}img\{1}", HttpContext.Current.Server.MapPath("~"), Url);
			imageFile = System.Drawing.Image.FromFile(imagePath);
		}
		


		private string GiveHtmlDimmensionStyle(string dimensionName, float givenSize)
		{
			if (dimensionName.ToLower() == "width")
				givenSize -= Helper.CurrentParams.PaddingRight + Helper.CurrentParams.PaddingLeft;
			else if (dimensionName.ToLower() == "height")
				givenSize -= Helper.CurrentParams.PaddingTop + Helper.CurrentParams.PaddingBottom;

			return string.Format("{0}: {1}px;", dimensionName, givenSize);
		}
	


		public override string RenderHtml(float givenSize)
		{
			return string.Format(@"
<div class='image'>
	<img src='/img/{0}' style='{1} margin: {2}px {3}px {4}px {5}px;' />
</div>"
				, Url, GiveHtmlDimmensionStyle(parent.CalculatedSizeName, givenSize)
				, Helper.CurrentParams.PaddingTop
				, Helper.CurrentParams.PaddingRight
				, Helper.CurrentParams.PaddingBottom
				, Helper.CurrentParams.PaddingLeft
				);
		}

		public override string ToString()
		{
			return string.Format("{0}('{1}') [{2} x {3}]", this.GetType().Name, Url, Width, Height);
		}

		public override void SetWidthHeightForChild(Image image, out float _outerWidth, out float _outerHeight)
		{
			throw new ArgumentException(argumentExceptionMessage);
		}
		public override float SetGivenSizeForChild(BaseStoryboardObject childObject)
		{
			throw new ArgumentException(argumentExceptionMessage);
		}
	}
}
