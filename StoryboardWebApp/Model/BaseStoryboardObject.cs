﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace StoryboardWebApp.Model
{
	public abstract class BaseStoryboardObject
	{
		protected string title;
		protected BaseStoryboardObject parent { get; set; }
		protected List<BaseStoryboardObject> Childs = new List<BaseStoryboardObject>();



		protected abstract float calculatedSize { get; }



		public abstract string RenderHtml(float givenSize);
		public abstract void SetWidthHeightForChild(Image image, out float _outerWidth, out float _outerHeight);

		public abstract float Width { get; }
		public abstract float Height { get; }

		public abstract string GivenSizeName { get; }
		public abstract string CalculatedSizeName { get; }

		public abstract float SetGivenSizeForChild(BaseStoryboardObject childObject);



		float _givenSize = 0;
		public float GivenSize
		{
			get
			{
				if (_givenSize == 0)
					_givenSize = parent.SetGivenSizeForChild(this);
				return _givenSize;
			}
			set { _givenSize = value; }
		}

		float _innerWhRelation = 0;
		public virtual float InnerWhRelation
		{
			get
			{
				if (_innerWhRelation == 0)
					foreach (var item in Childs)
						_innerWhRelation += item.InnerWhRelation;

				return _innerWhRelation;
			}
		}

		float _innerHwRelation = 0;
		public virtual float InnerHwRelation
		{
			get
			{
				if (_innerHwRelation == 0)
					foreach (var item in Childs)
						_innerHwRelation += item.InnerHwRelation;

				return _innerHwRelation;
			}
		}

		float _outerWhRelation = 0;
		public virtual float OuterWhRelation
		{
			get
			{
				if (_outerWhRelation == 0)
					foreach (var item in Childs)
						_outerWhRelation += item.OuterWhRelation;

				return _outerWhRelation;
			}
		}

		float _outerHwRelation = 0;
		public virtual float OuterHwRelation
		{
			get
			{
				if (_outerHwRelation == 0)
					foreach (var item in Childs)
						_outerHwRelation += item.OuterHwRelation;

				return _outerHwRelation;
			}
		}



		protected string RenderChilds()
		{
			StringBuilder result = new StringBuilder();

			foreach (var item in Childs)
			{
				result.Append(item.RenderHtml(calculatedSize));
			}

			return result.ToString();
		}



		public BaseStoryboardObject Add(BaseStoryboardObject obj)
		{
			this.Childs.Add(obj);
			obj.parent = this;
			return this;
		}

		public StringBuilder ToStringRecoursive()
		{
			return this.ToStringRecoursive(0);
		}
		StringBuilder ToStringRecoursive(int depth)
		{
			depth++;
			StringBuilder sbResult = new StringBuilder(this.ToString());

			foreach (var item in Childs)
			{
				sbResult.AppendFormat("<br />{1}{0}", item.ToStringRecoursive(depth), Helper.GetDashes(depth));
			}

			return sbResult;

		}

		public override string ToString()
		{
			return string.Format("{0}({1}), givenSize({2}) = {3}, calculatedSize({4}) = {5}"
				, this.GetType().Name, title, GivenSizeName, GivenSize, CalculatedSizeName, calculatedSize);
		}

	}
}