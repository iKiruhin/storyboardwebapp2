﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoryboardWebApp.Model
{
	public class Row : BaseStoryboardObject
	{
		/// <summary>
		/// width of row without paddings (summ of childImage widths)
		/// </summary>
		float innerGivenSize
		{
			get { return GivenSize - Childs.Count * (Helper.CurrentParams.PaddingLeft + Helper.CurrentParams.PaddingRight); }
		}


		protected override float calculatedSize
		{
			get { return Height; }
		}



		public override float Height
		{
			get
			{
				return Width / OuterWhRelation;
			}
		}
		public override float Width
		{
			get { return GivenSize; }
		}
		public override float OuterHwRelation
		{
			get
			{
				return 1 / OuterWhRelation;
			}
		}
		public override string GivenSizeName
		{
			get { return "width"; }
		}
		public override string CalculatedSizeName
		{
			get { return "height"; }
		}



		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="title"></param>
		public Row(string title)
		{
			this.title = title;
		}



		public override string RenderHtml(float givenWidth)
		{
			this.GivenSize = givenWidth;

			string result = RenderChilds();
			result = string.Format("<div id='{1}' class='row sbo' style='height: {2}px;'>{0}</div>", result, title, Height);
			//result = string.Format("<div id='{1}' class='row sbo' style='width: {2}px; height: {3}px;'>{0}</div>", result, title, Width, Height);
			return result;
		}

		public override void SetWidthHeightForChild(Image childImage, out float _outerWidth, out float _outerHeight)
		{
			float oWidth = 0, oHeight = 0;

			oWidth = innerGivenSize * childImage.InnerWhRelation / InnerWhRelation;

			oHeight = oWidth * childImage.InnerHwRelation;

			oWidth += Helper.CurrentParams.PaddingLeft + Helper.CurrentParams.PaddingRight;
			oHeight += Helper.CurrentParams.PaddingTop + Helper.CurrentParams.PaddingBottom;

			_outerWidth = oWidth;
			_outerHeight = oHeight;
		}

		/// <summary>
		/// метод не дописан. работает верно только для случая с нулевыми отступами
		/// </summary>
		/// <param name="childObject"></param>
		/// <returns></returns>
		public override float SetGivenSizeForChild(BaseStoryboardObject childObject)
		{
			float x = 0;
			float localOuterWh = InnerWhRelation + x;

			return GivenSize / localOuterWh; 
		
			//float childSize = summaryHorizontalPaddings + summaryChildWidths;



			//return childSize;
		}
	}
}