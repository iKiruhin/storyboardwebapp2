﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoryboardWebApp.Model
{
	public class Column : BaseStoryboardObject
	{
		/// <summary>
		/// height of row without paddings (summ of childImage widths)
		/// </summary>
		float innerGivenSize
		{
			get { return GivenSize - Childs.Count * (Helper.CurrentParams.PaddingTop + Helper.CurrentParams.PaddingBottom); }
		}



		protected override float calculatedSize
		{
			get { return Width; }
		}



		public override float Width
		{
			get
			{
				return Height / OuterHwRelation;
			}
		}
		public override float Height
		{
			get { return GivenSize; }
		}
		public override float OuterWhRelation
		{
			get
			{
				return 1 / OuterHwRelation;
			}
		}
		public override string GivenSizeName
		{
			get { return "height"; }
		}
		public override string CalculatedSizeName
		{
			get { return "width"; }
		}


		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="title"></param>
		public Column(string title)
		{
			this.title = title;
		}



		public override string RenderHtml(float givenHeight)
		{
			this.GivenSize = givenHeight;

			string result = RenderChilds();
			result = string.Format("<div id='{1}' class='column sbo' style='width: {2}px; '>{0}</div>", result, title, Width);
			//result = string.Format("<div id='{1}' class='column sbo' style='width: {2}px; height: {3}px;'>{0}</div>", result, title, Width, Height);
			return result;
		}

		public override void SetWidthHeightForChild(Image childImage, out float _outerWidth, out float _outerHeight)
		{
			float oWidth = 0, oHeight = 0;

			oHeight = innerGivenSize * childImage.InnerHwRelation / InnerHwRelation;

			oWidth = oHeight * childImage.InnerWhRelation;

			oHeight += Helper.CurrentParams.PaddingTop + Helper.CurrentParams.PaddingBottom;
			oWidth += Helper.CurrentParams.PaddingLeft + Helper.CurrentParams.PaddingRight;

			_outerWidth = oWidth;
			_outerHeight = oHeight;
		}

		/// <summary>
		/// метод не дописан. работает верно только для случая с нулевыми отступами
		/// </summary>
		/// <param name="childObject"></param>
		/// <returns></returns>
		public override float SetGivenSizeForChild(BaseStoryboardObject childObject)
		{
			return GivenSize / InnerHwRelation;
			//float childSize = summaryVerticalPaddings + summaryChildHeights;



			//return childSize;
		}
	}
}