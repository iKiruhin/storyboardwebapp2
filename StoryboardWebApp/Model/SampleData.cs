﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoryboardWebApp.Model
{
	public class SampleData
	{
		public static BaseStoryboardObject ObviousSampleData_SingleImage
		{
			get
			{
				var r = new Row("r");

				r.Add(new Image("100x100.jpg"));

				return r;
			}
		}
		public static BaseStoryboardObject ObviousSampleData_SingleRow
		{
			get
			{
				var r = new Row("r");

				r.Add(new Image("100x100.jpg"))
					.Add(new Image("100x200.jpg"))
					.Add(new Image("200x100.jpg"))
					.Add(new Image("100x100.jpg"))
					.Add(new Image("100x200.jpg"))
					;

				return r;
			}
		}
		public static BaseStoryboardObject ObviousSampleData_SingleColumn
		{
			get
			{
				var c = new Column("c");

				c.Add(new Image("100x100.jpg"))
					.Add(new Image("100x100.jpg"))
					.Add(new Image("100x100.jpg"))
					.Add(new Image("100x100.jpg"))
					.Add(new Image("100x100.jpg"))
					;

				return c;
			}
		}
		public static BaseStoryboardObject ObviousSampleData_NestedColumn
		{
			get
			{
				var r1 = new Row("r1");
				var c1 = new Column("c1");

				r1
					.Add(c1)
					.Add(new Image("100x400.jpg"))
					;

				c1
					.Add(new Image("100x100.jpg"))
					.Add(new Image("100x100.jpg"))
					;

				return r1;
			}
		}
		public static BaseStoryboardObject ObviousSampleData_NestedRow
		{
			get
			{
				var c1 = new Column("c1");
				var r1 = new Row("r1");

				c1
					.Add(r1)
					.Add(new Image("400x100.jpg"))
					;

				r1
					.Add(new Image("100x100.jpg"))
					.Add(new Image("100x100.jpg"))
					;

				return c1;
			}
		}

		public static BaseStoryboardObject SampleData_SingleImage
		{
			get
			{
				var r = new Row("r");

				r.Add(new Image("1.jpg"));

				return r;
			}
		}
		public static BaseStoryboardObject SampleData_SingleRow
		{
			get
			{
				var r = new Row("r");

				r.Add(new Image("1.jpg"))
					.Add(new Image("2.jpg"))
					.Add(new Image("3.jpg"))
					.Add(new Image("4.jpg"))
					.Add(new Image("5.jpg"))
					;

				return r;
			}
		}
		public static BaseStoryboardObject SampleData_SingleColumn
		{
			get
			{
				var c = new Column("c");

				c.Add(new Image("1.jpg"))
					.Add(new Image("2.jpg"))
					.Add(new Image("3.jpg"))
					.Add(new Image("4.jpg"))
					.Add(new Image("5.jpg"))
					;

				return c;
			}
		}
		public static BaseStoryboardObject SampleData_ComplexMultiline
		{
			get
			{
				var r1 = new Row("r1");
				var c1 = new Column("c1");
				var r2 = new Row("r2");
				var c2 = new Column("c2");
				var r3 = new Row("r3");

				r1.Add(new Image("12.jpg"))
					.Add(c1)
					.Add(new Image("13.jpg"))
					;

				c1.Add(r2)
					.Add(new Image("7.jpg"))
					;

				r2.Add(new Image("4.jpg"))
					.Add(c2);

				c2.Add(r3)
					.Add(new Image("10.jpg"))
					;

				r3.Add(new Image("6.jpg"))
					.Add(new Image("5.jpg"));

				return r1;
			}
		}
		public static BaseStoryboardObject SampleData_ComplexMultiline2
		{
			get
			{
				var r1 = new Row("r1");
				var r2 = new Row("r2");
				var r3 = new Row("r3");
				var r4 = new Row("r4");
				var r5 = new Row("r5");
				var r6 = new Row("r6");
				var r7 = new Row("r7");
				var c1 = new Column("c1");
				var c2 = new Column("c2");
				var c3 = new Column("c3");
				var c4 = new Column("c4");
				var c5 = new Column("c5");

				r1.Add(new Image("1.jpg")).Add(c1).Add(c2).Add(new Image("19.jpg"));
				c2.Add(new Image("17.jpg")).Add(new Image("18.jpg"));
				c1.Add(r2).Add(r3).Add(new Image("16.jpg"));
				r2.Add(c3).Add(new Image("5.jpg"));
				c3.Add(r4).Add(new Image("4.jpg"));
				r4.Add(new Image("2.jpg")).Add(new Image("3.jpg"));
				r3.Add(new Image("6.jpg")).Add(c4).Add(c5);
				c4.Add(r5).Add(new Image("10.jpg")).Add(r6);
				r5.Add(new Image("7.jpg")).Add(new Image("8.jpg")).Add(new Image("9.jpg"));
				r6.Add(new Image("11.jpg")).Add(new Image("12.jpg"));
				c5.Add(r7).Add(new Image("15.jpg"));
				r7.Add(new Image("13.jpg")).Add(new Image("14.jpg"));

				return r1;
			}
		}
		public static BaseStoryboardObject SampleData_NestedColumn
		{
			get
			{
				var r1 = new Row("r1");
				var c1 = new Column("c1");

				r1
					.Add(c1)
					.Add(new Image("12.jpg"))
					;

				c1.Add(new Image("2.jpg"))
					.Add(new Image("7.jpg"))
					;

				return r1;
			}
		}
		public static BaseStoryboardObject SampleData_NestedRow
		{
			get
			{
				var c1 = new Column("c1");
				var r1 = new Row("r1");

				c1.Add(r1)
					.Add(new Image("10.jpg"))
					;

				r1.Add(new Image("3.jpg"))
					.Add(new Image("7.jpg"))
					;

				return c1;
			}
		}
	}
}