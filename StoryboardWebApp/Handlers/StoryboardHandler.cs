﻿using StoryboardWebApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace StoryboardWebApp.Handlers
{
	public class StoryboardHandler
	{
		internal static string Draw(Model.BaseStoryboardObject obj, int storyboardSize)
		{
			StoryboardParameters sbParams = new StoryboardParameters();
			sbParams.Size = storyboardSize;
			return Draw(obj, sbParams);
		}

		internal static string Draw(BaseStoryboardObject obj, int storyboardSize, StoryboardParameters storyboardParameters)
		{
			StoryboardParameters sbParams = storyboardParameters;
			sbParams.Size = storyboardSize;
			return Draw(obj, sbParams);
		}

		internal static string Draw(Model.BaseStoryboardObject obj, Model.StoryboardParameters storyboardParameters)
		{
			Helper.CurrentParams = storyboardParameters;

			StringBuilder result = new StringBuilder();

//<div class='storyboard' style='width: {3}px; height: {4}px;'>
			result.AppendFormat(@"
<div class='storyboard' style='{0}: {1}px;'>
	{2}
</div>"
				, obj.GivenSizeName
				, storyboardParameters.Size
				, obj.RenderHtml(storyboardParameters.Size)
			);

#if DEBUG
			result.AppendFormat(@"
<div class='storyboardDescription'>
	{0}<br />
	<strong>Obj</strong><br />
	{1}
</div>"
				, storyboardParameters.ToString(obj.GivenSizeName)
				, obj.ToStringRecoursive()
			);
#endif

			return result.ToString();
		}


	}
}