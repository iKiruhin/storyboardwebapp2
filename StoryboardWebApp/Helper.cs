﻿using StoryboardWebApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace StoryboardWebApp
{
	public class Helper
	{
		internal static string GetDashes(int depth)
		{
			StringBuilder result = new StringBuilder();
			for (int i = 0; i < depth; i++)
			{
				result.Append("----");
			}

			return result.ToString();
		}

		static StoryboardParameters currentParams;
		public static StoryboardParameters CurrentParams
		{
			get { return Helper.currentParams; }
			set { Helper.currentParams = value; }
		}
	}
}