﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using StoryboardWebApp.Handlers;
using StoryboardWebApp.Model;

namespace StoryboardWebApp
{
	public partial class DrawStoryBoard : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			ltStoryboard0.Text = StoryboardHandler.Draw(
				SampleData.SampleData_SingleImage
				, StoryboardParameters.PaddingsComplex
				);

			ltStoryboard1.Text = StoryboardHandler.Draw(
				SampleData.SampleData_SingleRow
				, StoryboardParameters.PaddingsCustom
				);

			ltStoryboard2.Text = StoryboardHandler.Draw(
				SampleData.SampleData_SingleColumn
				, StoryboardParameters.Paddings10
				);

			ltStoryboard3.Text = StoryboardHandler.Draw(
				SampleData.SampleData_NestedRow
				, StoryboardParameters.Paddings0
				);

			ltStoryboard4.Text = StoryboardHandler.Draw(
				SampleData.SampleData_NestedColumn
				, StoryboardParameters.Paddings0
				);
			ltStoryboard5.Text = StoryboardHandler.Draw(
				SampleData.SampleData_ComplexMultiline
				, 800
				//, StoryboardParameters.Paddings0
				);

			ltStoryboard6.Text = StoryboardHandler.Draw(
				SampleData.SampleData_ComplexMultiline2
				, 1500
				//, StoryboardParameters.Paddings10
				);

		}

	}
}
