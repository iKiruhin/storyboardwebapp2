﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DrawStoryBoard.aspx.cs" Inherits="StoryboardWebApp.DrawStoryBoard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<link href="css/storyboard.css" rel="stylesheet" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="The description of my page" />
</head>
<body>
	<form id="form1" runat="server">
		<div class="storyboardContainer">
			<asp:Literal ID="ltStoryboard0" runat="server"></asp:Literal>
		</div>
		<div class="storyboardContainer">
			<asp:Literal ID="ltStoryboard1" runat="server"></asp:Literal>
		</div>
		<div class="storyboardContainer">
			<asp:Literal ID="ltStoryboard2" runat="server"></asp:Literal>
		</div>
		<div class="storyboardContainer">
			<asp:Literal ID="ltStoryboard3" runat="server"></asp:Literal>
		</div>
		<div class="storyboardContainer">
			<asp:Literal ID="ltStoryboard4" runat="server"></asp:Literal>
		</div>
		<div class="storyboardContainer">
			<asp:Literal ID="ltStoryboard5" runat="server"></asp:Literal>
		</div>
		<div class="storyboardContainer">
			<asp:Literal ID="ltStoryboard6" runat="server"></asp:Literal>
		</div>
	</form>
</body>
</html>
